package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinSonarTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenkinSonarTestProjectApplication.class, args);
	}

}
